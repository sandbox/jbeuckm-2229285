<?php
/**
 * @file
 * test_content_type.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function test_content_type_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create complex_content content'.
  $permissions['create complex_content content'] = array(
    'name' => 'create complex_content content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'save file information'.
  $permissions['save file information'] = array(
    'name' => 'save file information',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'services',
  );

  return $permissions;
}

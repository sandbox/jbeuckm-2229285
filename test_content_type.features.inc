<?php
/**
 * @file
 * test_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function test_content_type_node_info() {
  $items = array(
    'complex_content' => array(
      'name' => t('Complex Content'),
      'base' => 'node_content',
      'description' => t('A collection of many field types for testing.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
